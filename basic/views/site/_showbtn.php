<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php if ($result != false): ?>

    <p>Вы выиграли <?= $result->what ?></p>

    <?php $form = ActiveForm::begin([
        'action' => '/site/approve',
        'id' => 'resalt-to-approve-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= Html::hiddenInput('table_name', $result->table_name); ?>
    <?= Html::hiddenInput('model_id', $result->model_id); ?>
    <?= Html::submitButton('Взять подарок',
        ['class' => 'btn btn-default btn-lg', 'name' => 'yes']) ?>
    <?= Html::submitButton('Отказаться',
        ['class' => 'btn btn-default btn-lg', 'name' => 'no']) ?>

    <?php if ($result->table_name == 'prize_bonus'): ?>

        <?= Html::submitButton('Конвертировать в '.$result->getMoneySum($prize_model).'$',
            ['class' => 'btn btn-default btn-lg', 'name' => 'convert']) ?>

    <? endif; ?>
    <?php ActiveForm::end(); ?>

<?php else: ?>

    <?php $form = ActiveForm::begin([
        'id' => 'prize-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= Html::submitButton('<span class="glyphicon glyphicon-star"></span> Get the prize!',
        ['class' => 'btn btn-default btn-lg', 'name' => 'prize-button']) ?>
    <?php ActiveForm::end(); ?>

<? endif; ?>



