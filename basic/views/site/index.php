<?php

/* @var $this yii\web\View */

$this->title = 'Raffle prizes';
?>
<div class="site-index">

    <?php if (!Yii::$app->user->isGuest): ?>

        <?= $this->render('_showbtn', ['result' => $result, 'prize_model' => $prize_model]) ?>

    <? endif; ?>

</div>
