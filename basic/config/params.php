<?php

return [
    'adminEmail' => 'admin@example.com',
    'prizes_types' => [
        'money' => [
            'max' => 1000,
            'min' => 20
        ],
        'bonus' => [
            'max' => 5000,
            'min' => 500
        ],
        'thing'
    ],
    '1bonus_ratio' => 0.5,
    'all_money' => 1000000
];
