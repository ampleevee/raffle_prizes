<?php
/**
 * Created by PhpStorm.
 * User: evgenijampleev
 * Date: 2019-02-12
 * Time: 16:42
 */

namespace app\commands;

use yii\console\Controller;
use app\models\PrizeMoney;

class MoneysendController extends Controller
{

    public function actionIndex($number_of_items = 2, $time_between = 2)
    {
        echo "-------------------------------------\n";
        echo "-------------------------------------\n";
        echo "Скрипт будет отправлять пачки по " . $number_of_items . " транзакций с интервалом " . $time_between . " секунд\n";
        echo "-------------------------------------\n";
        echo "-------------------------------------\n";
        echo "Получаем ближайшие не обработанные транзакции\n";

        $are_transactions_to_send = true;

        while ($are_transactions_to_send == true) :

            $transactions = PrizeMoney::find()
                ->where(['sended' => 0])
                ->andWhere(['agreed' => 1])
                ->limit($number_of_items)
                ->all();

            $number_of_items_in_packege = count($transactions);
            echo "Количество полученных транзакций: " . $number_of_items_in_packege . "\n";

            if ($number_of_items_in_packege > 0) {
                echo "Получили " . $number_of_items_in_packege . " транзакций, которые необходимо отправить\n";
                echo "Начинаем отправку..\n";

                for ($i = 0; $i < $number_of_items_in_packege; $i++) {
                    $transactions[$i]->sended = rand(0, 1);
                    $transactions[$i]->sended_last_try = date("Y-m-d H:i:s");
                    $transactions[$i]->save();

                    echo "Транзакция с id " . $transactions[$i]->id;
                    if ($transactions[$i]->sended == 0) {
                        echo " не отправлена, произошла ошибка\n";
                    } else {
                        echo " успешно отправлена в банк\n";
                    }

                }

                time_nanosleep($time_between, 0);

            } elseif ($number_of_items_in_packege == 0) {

                echo "Все транзакции обработаны, скрипт вынужден завершиться\n";
                $are_transactions_to_send = false;
            }
        endwhile;
    }

}