<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "thing".
 *
 * @property int $id
 * @property string $name
 * @property int $quantity
 *
 * @property PrizeThing[] $prizeThings
 */
class Thing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'thing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['quantity'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizeThings()
    {
        return $this->hasMany(PrizeThing::className(), ['thing_id' => 'id']);
    }
}
