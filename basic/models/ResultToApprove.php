<?php
/**
 * Created by PhpStorm.
 * User: evgenijampleev
 * Date: 2019-02-11
 * Time: 22:47
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "prize_thing".
 *
 * @property string $what
 * @property string $table_name
 * @property int $model_id
 *
 */
class ResultToApprove
{
    public $what;
    public $table_name;
    public $model_id;

    public function getMoneySum(PrizeBonus $prize_bonus_model)
    {
        return $prize_bonus_model->sum * Yii::$app->params['1bonus_ratio'];
    }

}