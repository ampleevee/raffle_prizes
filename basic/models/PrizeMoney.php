<?php

namespace app\models;

use app\interfaces\PrizeInterface;
use Yii;

/**
 * This is the model class for table "prize_money".
 *
 * @property int $id
 * @property double $sum
 * @property string $date
 * @property int $user_id
 * @property int $agreed
 * @property int $sended
 * @property string $sended_last_try
 *
 * @property User $user
 */
class PrizeMoney extends \yii\db\ActiveRecord implements PrizeInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize_money';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum', 'date', 'user_id', 'agreed'], 'required'],
            [['sum'], 'number'],
            [['date', 'sended_last_try'], 'safe'],
            [['user_id', 'agreed', 'sended'], 'integer'],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'date' => 'Date',
            'user_id' => 'User ID',
            'agreed' => 'Agreed',
            'sended' => 'Sended',
            'sended_last_try' => 'Sended Last Try',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function fill_the_data($sum = false)
    {

        if (!$sum) {
            $this->sum = rand(Yii::$app->params['prizes_types']['money']['min'],
                Yii::$app->params['prizes_types']['money']['max']);
        } else {
            $this->sum = $sum;
        }

        $this->date = date("Y-m-d H:i:s");
        $this->user_id = Yii::$app->user->id;
        $this->agreed = 0;
        $this->save();
    }
}
