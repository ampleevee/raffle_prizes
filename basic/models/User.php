<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property double $balance_bonus
 *
 * @property PrizeBonus[] $prizeBonuses
 * @property PrizeMoney[] $prizeMoneys
 * @property PrizeThing[] $prizeThings
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['balance_bonus'], 'number'],
            [['username', 'password'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'balance_bonus' => 'Balance Bonus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizeBonuses()
    {
        return $this->hasMany(PrizeBonus::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizeMoneys()
    {
        return $this->hasMany(PrizeMoney::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizeThings()
    {
        return $this->hasMany(PrizeThing::className(), ['user_id' => 'id']);
    }
}