<?php
/**
 * Created by PhpStorm.
 * User: evgenijampleev
 * Date: 2019-02-12
 * Time: 09:43
 */

namespace app\models;

use app\models\PrizeMoney;
use app\models\PrizeBonus;
use app\models\PrizeThing;
use Yii;


class PrizeCountings
{
    public static function generate_prize()
    {
        $type_prize = rand(1, count(Yii::$app->params['prizes_types']));
        $prize_model = false;

        switch ($type_prize) {
            case 1:
                $prize_model = new PrizeMoney();
                break;

            case 2:
                $prize_model = new PrizeBonus();
                break;

            case 3:
                $prize_model = new PrizeThing();
                break;

        }
        $prize_model->fill_the_data();
        return $prize_model;
    }

    public static function generate_result_for_view($prize_model)
    {
        $result = new ResultToApprove();
        $table = $prize_model::tableName();

        switch ($table) {
            case 'prize_money':
                $result->what = $prize_model->sum . ' $';
                $result->table_name = $table;
                $result->model_id = $prize_model->id;
                break;
            case 'prize_bonus':
                $result->what = $prize_model->sum . ' бонусов';
                $result->table_name = $table;
                $result->model_id = $prize_model->id;
                break;
            case 'prize_thing':
                $result->what = $prize_model->thing->name;
                $result->table_name = $table;
                $result->model_id = $prize_model->id;
                break;
        }

        return $result;
    }

    public static function get_approve($data)
    {
        if (isset($data['yes']) && !isset($data['convert'])) {

            switch ($data['table_name']) {
                case 'prize_money':
                    $model = PrizeMoney::findOne($data['model_id']);
                    $model->agreed = 1;
                    if ($model->save()) {
                        return true;
                    }
                    return false;

                case 'prize_bonus':
                    $model = PrizeBonus::findOne($data['model_id']);
                    $model->agreed = 1;
                    if ($model->save()) {
                        return true;
                    }
                    return false;
                case 'prize_thing':
                    $model = PrizeThing::findOne($data['model_id']);
                    $model->agreed = 1;
                    if ($model->save()) {
                        return true;
                    }
                    return false;
            }

        } elseif (isset($data['convert'])) {

            if (PrizeCountings::convert_bonus_to_money($data['model_id'])) {
                return true;
            }
            return false;
        }
    }

    public static function convert_bonus_to_money($bonus_model_id)
    {
        $model = PrizeBonus::findOne($bonus_model_id);
        $model->agreed = 0;
        $money_model = new PrizeMoney();
        $money_model->fill_the_data($model->sum * Yii::$app->params['1bonus_ratio']);
        $money_model->agreed = 1;

        if ($money_model->save()) {

            $model->is_converted_to_money_id = $money_model->id;
            $model->save();
            return true;
        }
        return false;
    }
}