<?php

namespace app\models;

use app\interfaces\PrizeInterface;
use Yii;

/**
 * This is the model class for table "prize_thing".
 *
 * @property int $id
 * @property int $thing_id
 * @property string $date
 * @property int $user_id
 * @property int $agreed
 * @property int $sended
 *
 * @property Thing $thing
 * @property User $user
 */
class PrizeThing extends \yii\db\ActiveRecord implements PrizeInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize_thing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['thing_id', 'date', 'user_id', 'agreed'], 'required'],
            [['thing_id', 'user_id', 'agreed', 'sended'], 'integer'],
            [['date'], 'safe'],
            [
                ['thing_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Thing::className(),
                'targetAttribute' => ['thing_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thing_id' => 'Thing ID',
            'date' => 'Date',
            'user_id' => 'User ID',
            'agreed' => 'Agreed',
            'sended' => 'Sended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThing()
    {
        return $this->hasOne(Thing::className(), ['id' => 'thing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function fill_the_data()
    {

        $countThingsAll = (new yii\db\Query())
            ->from('thing')
            ->count();

        $this->thing_id = rand(1, $countThingsAll);
        $this->date = date("Y-m-d H:i:s");
        $this->user_id = Yii::$app->user->id;
        $this->agreed = 0;
        $this->save();
    }
}
