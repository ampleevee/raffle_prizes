<?php

namespace app\models;

use app\interfaces\PrizeInterface;
use Yii;

/**
 * This is the model class for table "prize_bonus".
 *
 * @property int $id
 * @property int $sum
 * @property string $date
 * @property int $user_id
 * @property int $agreed
 * @property int $sended
 * @property int $is_converted_to_money_id
 *
 * @property PrizeMoney $isConvertedToMoney
 * @property User $user
 */
class PrizeBonus extends \yii\db\ActiveRecord implements PrizeInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize_bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum', 'date', 'user_id', 'agreed'], 'required'],
            [['sum', 'user_id', 'agreed', 'sended', 'is_converted_to_money_id'], 'integer'],
            [['date'], 'safe'],
            [
                ['is_converted_to_money_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PrizeMoney::className(),
                'targetAttribute' => ['is_converted_to_money_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'date' => 'Date',
            'user_id' => 'User ID',
            'agreed' => 'Agreed',
            'sended' => 'Sended',
            'is_converted_to_money_id' => 'Is Converted To Money ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsConvertedToMoney()
    {
        return $this->hasOne(PrizeMoney::className(), ['id' => 'is_converted_to_money_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function fill_the_data()
    {

        $this->sum = rand(Yii::$app->params['prizes_types']['bonus']['min'],
            Yii::$app->params['prizes_types']['bonus']['max']);
        $this->date = date("Y-m-d H:i:s");
        $this->user_id = Yii::$app->user->id;
        $this->agreed = 0;
        $this->save();
    }
}
