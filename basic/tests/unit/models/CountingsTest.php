<?php

namespace models;

use app\models\PrizeCountings;
use app\models\PrizeMoney;
use app\models\PrizeBonus;

class CountingsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Тест на наличие данных в базе
     */
    public function testIsDataInDb()
    {
        $result = false;

        $sql = "SELECT LAST_INSERT_ID() FROM prize_bonus";
        $last_id = \Yii::$app->db->createCommand($sql)->execute();
        if ($last_id > 0) {
            $result = true;
        }
        expect_that($result);

    }

    public function testCreatePrizeMoney()
    {
        $money_model = new PrizeMoney();
        $money_model->sum = 100;
        $money_model->date = date('Y-m-d H:i:s');
        $money_model->user_id = 1;
        $money_model->agreed = 0;
        $money_model->sended = 0;
        expect_that($money_model->save());

    }

    public function testCreatePrizeBonus()
    {
        $bonus_model = new PrizeBonus();
        $bonus_model->sum = 8;
        $bonus_model->date = date('Y-m-d H:i:s');
        $bonus_model->user_id = 1;
        $bonus_model->agreed = 0;
        $bonus_model->sended = 0;
        expect_that($bonus_model->save());

    }

    public function testConverter()
    {
        $bonus_model = new PrizeBonus();
        $bonus_model->sum = 8;
        $bonus_model->date = date('Y-m-d H:i:s');
        $bonus_model->user_id = 1;
        $bonus_model->agreed = 0;
        $bonus_model->sended = 0;
        $bonus_model->save();

        if (PrizeCountings::convert_bonus_to_money($bonus_model->id)) {
            $money_model = PrizeMoney::findOne($bonus_model->is_converted_to_money_id);
            expect_that($money_model->sum === $bonus_model->sum * \Yii::$app->params['1bonus_ratio']);
        }

    }

}