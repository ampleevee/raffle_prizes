<?php

use yii\db\Migration;

/**
 * Class m190211_191325_addAgreed2
 */
class m190211_191325_addAgreed2 extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_money` 
ADD COLUMN `agreed` TINYINT NOT NULL AFTER `user_id`;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
