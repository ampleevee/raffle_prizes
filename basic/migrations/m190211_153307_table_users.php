<?php

use yii\db\Migration;

/**
 * Class m190211_153307_table_users
 */
class m190211_153307_table_users extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `raffle-prizes`.`user` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `email` VARCHAR(45) NOT NULL,
                `password` VARCHAR(45) NOT NULL,
                PRIMARY KEY (`id`));";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {
        $sql = "DROP TABLE `raffle-prizes`.`user`;";
        \Yii::$app->db->createCommand($sql)->execute();
    }
}
