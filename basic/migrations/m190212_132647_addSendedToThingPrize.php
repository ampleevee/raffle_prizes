<?php

use yii\db\Migration;

/**
 * Class m190212_132647_addSendedToThingPrize
 */
class m190212_132647_addSendedToThingPrize extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_thing` 
ADD COLUMN `sended` TINYINT NOT NULL DEFAULT 0 AFTER `agreed`;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
