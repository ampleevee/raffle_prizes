<?php

use yii\db\Migration;

/**
 * Class m190211_181037_addTableThing
 */
class m190211_181037_addTableThing extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `raffle-prizes`.`thing` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
