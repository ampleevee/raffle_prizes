<?php

use yii\db\Migration;

/**
 * Class m190211_181439_addFkUserMoney
 */
class m190211_181439_addFkUserMoney extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_money` 
ADD COLUMN `user_id` INT NOT NULL AFTER `date`,
ADD INDEX `fkMoneyUser_idx` (`user_id` ASC);
ALTER TABLE `raffle-prizes`.`prize_money` 
ADD CONSTRAINT `fkMoneyUser`
  FOREIGN KEY (`user_id`)
  REFERENCES `raffle-prizes`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
