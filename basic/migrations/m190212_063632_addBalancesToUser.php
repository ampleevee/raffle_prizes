<?php

use yii\db\Migration;

/**
 * Class m190212_063632_addBalancesToUser
 */
class m190212_063632_addBalancesToUser extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`user` 
ADD COLUMN `balance_money` FLOAT NOT NULL DEFAULT 0 AFTER `password`,
ADD COLUMN `balance_bonus` FLOAT NOT NULL DEFAULT 0 AFTER `balance_money`;
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
