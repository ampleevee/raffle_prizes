<?php

use yii\db\Migration;

/**
 * Class m190211_180806_addPrizeThingTable
 */
class m190211_180806_addPrizeThingTable extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `raffle-prizes`.`prize_thing` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `thing_id` INT NOT NULL,
  `date` DATETIME NULL,
  `user_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fkUserThing_idx` (`user_id` ASC),
  CONSTRAINT `fkUserThing`
    FOREIGN KEY (`user_id`)
    REFERENCES `raffle-prizes`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
