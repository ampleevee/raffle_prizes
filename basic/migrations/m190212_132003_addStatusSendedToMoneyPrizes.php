<?php

use yii\db\Migration;

/**
 * Class m190212_132003_addStatusSendedToMoneyPrizes
 */
class m190212_132003_addStatusSendedToMoneyPrizes extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_money` 
ADD COLUMN `sended` TINYINT NOT NULL DEFAULT 0 AFTER `agreed`,
ADD COLUMN `sended_last_try` DATETIME NULL AFTER `sended`;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
