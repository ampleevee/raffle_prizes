<?php

use yii\db\Migration;

/**
 * Class m190211_191246_addAgreed1
 */
class m190211_191246_addAgreed1 extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_bonus` 
DROP FOREIGN KEY `fkUser`;
ALTER TABLE `raffle-prizes`.`prize_bonus` 
CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL ,
ADD COLUMN `agreed` TINYINT NOT NULL AFTER `user_id`;
ALTER TABLE `raffle-prizes`.`prize_bonus` 
ADD CONSTRAINT `fkUser`
  FOREIGN KEY (`user_id`)
  REFERENCES `raffle-prizes`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
