<?php

use yii\db\Migration;

/**
 * Class m190211_181253_addFkThingPrizeThing
 */
class m190211_181253_addFkThingPrizeThing extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_thing` 
ADD INDEX `fkThingPrizeThing_idx` (`thing_id` ASC);
ALTER TABLE `raffle-prizes`.`prize_thing` 
ADD CONSTRAINT `fkThingPrizeThing`
  FOREIGN KEY (`thing_id`)
  REFERENCES `raffle-prizes`.`thing` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
