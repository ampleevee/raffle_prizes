<?php

use yii\db\Migration;

/**
 * Class m190212_133425_AddLimitsToThingsMoneyAndRatio1Bonus
 */
class m190212_133425_AddLimitsToThingsMoneyAndRatio1Bonus extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`thing` 
ADD COLUMN `quantity` INT NOT NULL DEFAULT 100 AFTER `name`;
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
