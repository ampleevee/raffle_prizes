<?php

use yii\db\Migration;

/**
 * Class m190211_181747_addDataThings
 */
class m190211_181747_addDataThings extends Migration
{
    public function safeUp()
    {
        $sql = "INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Шкатулка');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Плюшевый мишка');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Запонки');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Сорочка');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Дудочка');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Уточка кря');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Маленький гномик');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Гном');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Пёс Тузик');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Юла');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Бубенцы');
INSERT INTO `raffle-prizes`.`thing` (`name`) VALUES ('Автомобиль');
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
