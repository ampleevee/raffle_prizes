<?php

use yii\db\Migration;

/**
 * Class m190212_131414_deleteBalanceMoney
 */
class m190212_131414_deleteBalanceMoney extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`user` 
DROP COLUMN `balance_money`;
";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
