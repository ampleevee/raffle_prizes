<?php

use yii\db\Migration;

/**
 * Class m190211_191436_addAgreed3
 */
class m190211_191436_addAgreed3 extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_thing` 
DROP FOREIGN KEY `fkUserThing`;
ALTER TABLE `raffle-prizes`.`prize_thing` 
CHANGE COLUMN `date` `date` DATETIME NOT NULL ,
CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL ,
ADD COLUMN `agreed` TINYINT NOT NULL AFTER `user_id`;
ALTER TABLE `raffle-prizes`.`prize_thing` 
ADD CONSTRAINT `fkUserThing`
  FOREIGN KEY (`user_id`)
  REFERENCES `raffle-prizes`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
