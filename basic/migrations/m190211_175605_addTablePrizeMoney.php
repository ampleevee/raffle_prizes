<?php

use yii\db\Migration;

/**
 * Class m190211_175605_addTablePrizeMoney
 */
class m190211_175605_addTablePrizeMoney extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `raffle-prizes`.`prize_money` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sum` FLOAT NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id`));";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
