<?php

use yii\db\Migration;

/**
 * Class m190211_180114_addBonusesTable
 */
class m190211_180114_addBonusesTable extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `raffle-prizes`.`prize_bonus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sum` INT NOT NULL,
  `date` DATETIME NOT NULL,
  `user_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fkUser_idx` (`user_id` ASC),
  CONSTRAINT `fkUser`
    FOREIGN KEY (`user_id`)
    REFERENCES `raffle-prizes`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
