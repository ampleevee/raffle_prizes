<?php

use yii\db\Migration;

/**
 * Class m190211_170705_renameEmailToUsername
 */
class m190211_170705_renameEmailToUsername extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`user` 
CHANGE COLUMN `email` `username` VARCHAR(45) NOT NULL ;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
