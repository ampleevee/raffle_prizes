<?php

use yii\db\Migration;

/**
 * Class m190213_004606_AddIsConvertedToPrizeBonus
 */
class m190213_004606_AddIsConvertedToPrizeBonus extends Migration
{
    public function safeUp()
    {
        $sql = "ALTER TABLE `raffle-prizes`.`prize_bonus` 
ADD COLUMN `is_converted_to_money_id` INT NULL AFTER `sended`,
ADD INDEX `fkPrizeMoney_idx` (`is_converted_to_money_id` ASC);
ALTER TABLE `raffle-prizes`.`prize_bonus` 
ADD CONSTRAINT `fkPrizeMoney`
  FOREIGN KEY (`is_converted_to_money_id`)
  REFERENCES `raffle-prizes`.`prize_money` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {

    }
}
